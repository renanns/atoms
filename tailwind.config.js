/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,jsx,js}'],
  darkMode: 'class',
  theme: {
    screens: {
      md: { max: '830px' },
      // => @media (max-width: 800px) { ... }
      sm: { max: '585px' },
      // => @media (max-width: 585px) { ... }
      xsm: { max: '485px' },
      // => @media (max-width: 485px) { ... }
    },
    extend: {
      keyframes: {
        float: {
          '0%': {
            transform: 'translateY(0)',
          },
          '50%': {
            transform: 'translateY(-10px)',
          },
          '100%': {
            transform: 'translateY(0)',
          },
        },
        slideDown: {
          '0%': {
            transform: 'translateY(-60px)',
            opacity: '0',
          },
          '100%': {
            transform: 'translateY(0)',
            opacity: '1',
          },
        },
        floatIcons: {
          '0%, 100%': { transform: 'translate(0, 0)' },
          '25%': { transform: 'translate(20px, -20px)' },
          '50%': { transform: 'translate(-20px, 20px)' },
          '75%': { transform: 'translate(20px, 20px)' },
        },
      },
      animation: {
        float: 'float 3s ease-in-out infinite',
        slideDown: 'slideDown 1.5s ease-out',
        floatIcons: 'floatIcons 6s ease-in-out infinite',
      },
      delay: {
        0: '0s',
        1: '1s',
        2: '2s',
        3: '3s',
        4: '4s',
      },
      backgroundImage: {
        'home-atomo': "url('../public/assets/astro.png')",
        'about-atomo': "url('../public/assets/astro-about.png')",
        'contact-atomo': "url('../public/assets/contact-atomos.png')",
      },
      colors: {
        customBlue: '#38BDF8',
      },
    },
  },
  plugins: [],
};
