// Components
import ComponentHeader from './components/ComponentHeader';
import ComponentHome from './components/ComponentHome';
import ComponentAbout from './components/ComponentAbout';
import ComponentProjects from './components/ComponentProjects';

export default function App() {
  return (
    <div className="w-full h-full bg-slate-950">
      <ComponentHeader />
      <div id="Home" className="w-full p-2 mt-20 md:mt-32">
        <ComponentHome />
      </div>
      <div id="Quemsou" className="w-full p-2">
        <ComponentAbout />
      </div>
      <div id="Projetos" className="w-full p-2">
        <ComponentProjects />
      </div>
    </div>
  );
}
