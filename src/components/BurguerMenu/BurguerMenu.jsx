import { IoMenu } from "react-icons/io5";
import { IoClose } from "react-icons/io5";

import { useSelector } from "react-redux";

export const BurguerMenu  = ({handleOnClick}) => {
    const { menu } = useSelector(rootReducer => rootReducer.menuReducer);
    
    return (
        <button className="w-10 h-8 min-medium:hidden flex items-center justify-center rounded-full text-slate-50 dark:text-slate-700 bg-slate-300 dark:bg-slate-100"
          onClick={handleOnClick}>
            {!menu ? <IoMenu  size={20} color="#0F172A"/> : <IoClose size={20} color="#0F172A"/>}
        </button>
    );
};
