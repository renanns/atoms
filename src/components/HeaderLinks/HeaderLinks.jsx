const HeaderLinks  = () => {
    return (
        <nav className="w-full flex gap-4 justify-around max-medium:hidden">
          <a href="#home" className="text-sm font-medium text-slate-700 dark:text-slate-50 hover:text-blue-hover dark:hover:text-blue-hover"> Home </a>
          <a href="#about" className="text-sm font-medium text-slate-700 dark:text-slate-50 hover:text-blue-hover dark:hover:text-blue-hover"> About </a>
          <a href="#knowledge" className="text-sm font-medium text-slate-700 dark:text-slate-50 hover:text-blue-hover dark:hover:text-blue-hover"> Knowledge </a>
          <a href="#skills" className="text-sm font-medium text-slate-700 dark:text-slate-50 hover:text-blue-hover dark:hover:text-blue-hover"> Skills </a>
          <a href="#projects" className="text-sm font-medium text-slate-700 dark:text-slate-50 hover:text-blue-hover dark:hover:text-blue-hover"> Projects </a>
          <a href="#contact" className="text-sm font-medium text-slate-700 dark:text-slate-50 hover:text-blue-hover dark:hover:text-blue-hover"> Contact </a>
        </nav>
    );
};
export default HeaderLinks;