// Icons
import { SiJest } from 'react-icons/si';
import { IoLogoJavascript } from 'react-icons/io';
import { FaReact } from 'react-icons/fa';
import { SiStyledcomponents } from 'react-icons/si';

export default function ComponentAbout() {
  return (
    <div className="w-full h-screen flex items-center md:flex-col bg-slate-900">
      <div className="w-1/2 h-2/4 md:2/5 flex items-center justify-center">
        <div className=" flex items-center justify-center absolute">
          <div className="w-80 h-80 md:w-60 md:h-60 bg-customBlue z-1 rounded-full"></div>

          <div className="z-2 absolute">
            <img
              src="./assets/astro-about.png"
              alt="Logo Home Page"
              className="animate-float md:w-80 md:h-80 relative"
            />
            <div
              className="absolute bottom-14 right-5 md:right-2 w-12 h-12 md:w-9 md:h-9 animate-floatIcons bg-slate-700 flex items-center justify-center rounded-lg shadow-xl"
              style={{ animationDelay: '2s' }}
            >
              <FaReact color={'38BDF8'} size={24} />
            </div>
            <div
              className="absolute bottom-16 left-4 md:left-0 w-12 h-12 md:w-9 md:h-9 animate-floatIcons bg-slate-700 flex items-center justify-center rounded-lg shadow-xl"
              style={{ animationDelay: '1s' }}
            >
              <IoLogoJavascript color={'38BDF8'} size={24} />
            </div>
            <div
              className="absolute top-12 left-2 md:left-0 w-12 h-12 md:w-9 md:h-9 animate-floatIcons bg-slate-700 flex items-center justify-center rounded-lg shadow-xl"
              style={{ animationDelay: '2s' }}
            >
              <SiJest color={'38BDF8'} size={24} />
            </div>
            <div
              className="absolute top-14 right-4 md:right-1 w-12 h-12 md:w-9 md:h-9 animate-floatIcons bg-slate-700 flex items-center justify-center rounded-lg shadow-xl"
              style={{ animationDelay: '1s' }}
            >
              <SiStyledcomponents color={'38BDF8'} size={24} />
            </div>
          </div>
        </div>
      </div>

      <div className="w-1/2 h-full flex items-center justify-center md:w-full md:h-3/5">
        <div className="w-full flex flex-col justify-center gap-2 h-full md:items-center">
          <div className="relative w-full">
            <h1 className="text-2xl font-bold relative left-5 text-blue-50">Quem sou</h1>
            <h1 className="absolute bottom-3 text-2xl font-bold text-customBlue">
              Quem sou
            </h1>
          </div>
          <p className="text-base text-slate-500 italic sm:text-xs">
            Olá! Sou um Desenvolvedor de Sistemas e trabalho como Programador Web. Tenho
            paixão pelo desenvolvimento web e sempre busco aprender novas tecnologias para
            aprimorar minhas habilidades.
          </p>
          <p className="text-base text-slate-500 italic sm:text-xs">
            Tenho conhecimento em HTML, CSS, JavaScript, Typescript, ReactJS, Bootstrap,
            Tailwind CSS, Styled Components e Jest, com experiência na criação e
            refatoração de sites e sistemas institucionais.
          </p>
          <p className="text-base text-slate-500 italic sm:text-xs">
            Nas horas livres, foco em estudar novas linguagens de programação e
            tecnologias avançadas como Node, NextJs, Docker, PostgreSQL mantendo-me
            atualizada com as tendências da área, sou também intusiasta de Pentest onde
            utilizo o Kali Linux como meu SO.
          </p>
          <p className="text-base text-slate-500 italic sm:text-xs">
            Estou sempre em busca de aprender e dominar novas tecnologias, o que me mantém
            motivado e em constante evolução no desenvolvimento web.
          </p>
        </div>
      </div>
    </div>
  );
}
