import { addMenuScreen } from "../../redux/menu/slice";
import { useDispatch, useSelector } from "react-redux";

const Menu = () => {
    const menu = useSelector(state => state.menuReducer.menu);
    const dispatch = useDispatch();
    
    const handleClick = () => {
        dispatch(addMenuScreen(!menu));
    };

    return (
        <nav className='w-full h-72 flex flex-col gap-5 items-center pt-16 z-10 fixed bg-slate-50 dark:bg-slate-900 dark:border-b'>
            <a href="#home" className="text-sm font-medium text-slate-700 dark:text-slate-50 hover:text-blue-hover dark:hover:text-blue-hover" onClick={handleClick}> Home </a>
            <a href="#about" className="text-sm font-medium text-slate-700 dark:text-slate-50 hover:text-blue-hover dark:hover:text-blue-hover" onClick={handleClick}> About </a>
            <a href="#knowledge" className="text-sm font-medium text-slate-700 dark:text-slate-50 hover:text-blue-hover dark:hover:text-blue-hover" onClick={handleClick}> Knowledge </a>
            <a href="#skills" className="text-sm font-medium text-slate-700 dark:text-slate-50 hover:text-blue-hover dark:hover:text-blue-hover" onClick={handleClick}> Skills </a>
            <a href="#projects" className="text-sm font-medium text-slate-700 dark:text-slate-50 hover:text-blue-hover dark:hover:text-blue-hover" onClick={handleClick}> Projects </a>
            <a href="#contact" className="text-sm font-medium text-slate-700 dark:text-slate-50 hover:text-blue-hover dark:hover:text-blue-hover" onClick={handleClick}> Contact </a>
        </nav>
    )
};
export default Menu;