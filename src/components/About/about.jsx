import AboutInfos from '../AboutInfo/AboutInfo';

export const About = () => {
    return (
        <div className="h-full w-full flex">
            <div className="w-full h-full flex justify-center items-center max-medium:flex-col">       
                <div className="w-full h-full flex justify-center items-center">
                    <div className="w-full h-full flex flex-col gap-5 px-3 py-3">
                        <div className="relative mt-1">
                            <h1 className="text-5xl font-bold relative left-5 text-blue-200"> About </h1>
                            <h1 className="absolute bottom-4 text-5xl font-bold text-blue-500"> About </h1>
                        </div>


                        <div className="w-full h-full flex mt-5 h-3/6">
                            <AboutInfos
                                name={"Name:"}
                                dateBirth={"Date of birth:"}
                                address={"Address:"}
                                cep={"CEP:"} 
                                email={"E-mail:"}
                                phone={"Phone:"}
                                font={"text-blue-500 font-bold"}
                            />

                            <AboutInfos
                                name={"Renan Nascimento da Silva"}
                                dateBirth={"January - 30, 1999"}
                                address={"Campo Limpo - SP / Brasil"}
                                cep={"05849-280"} 
                                email={"atomsfrontend@gmail.com"}
                                phone={"(11) 94501-5885"}
                                font={"text-slate-800 dark:text-slate-50"}
                            />
                        </div>

                        <div className="mt-5 w-full flex justify-center items-center h-1/4">
                            <button className="bg-blue-500 px-5 py-3 rounded-xl text-slate-50 font-bold">
                                Download CV
                            </button>
                        </div>
                    </div>
                </div>
                <div className="w-full h-full flex justify-center items-center max-medium:hidden">
                    <div className="w-full h-full bg-about-atomo bg-center bg-no-repeat"></div>
                </div>
            </div>
        </div>
    )
};