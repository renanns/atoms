const AboutInfos = ({name, dateBirth, address, cep, email, phone, font}) => {
    return (
      <div className={`w-full flex flex-col justify-between items-start gap-1 max-small:text-xm`}>
          <p className={`${font}`}> {name} </p>
          <p className={`${font}`}> {dateBirth} </p>
          <p className={`${font}`}> {address} </p>
          <p className={`${font}`}> {cep} </p>
          <p className={`${font}`}> {email} </p>
          <p className={`${font}`}> {phone} </p>
      </div>
    )
};
export default AboutInfos;
  