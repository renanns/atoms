import React from "react";

const FooterLinks = ({icon, text, id}) => {
  return (
    <div className="w-full h-auto flex flex-col justify-center items-center gap-2">
        <div className="w-5/6 flex items-center justify-center gap-2 py-2">
            <div className="w-full h-auto flex items-center justify-start gap-2">
                {icon}
                <a href={id} className="hover:cursor-pointer dark:text-slate-50"> {text} </a>
            </div>
        </div>
    </div>
  )
};

export default FooterLinks;
