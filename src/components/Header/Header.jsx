import { BurguerMenu } from "../BurguerMenu/BurguerMenu";
import { HeaderButton } from "../HeaderButton/HeaderButton";
import HeaderLinks from "../HeaderLinks/HeaderLinks";

const Header = ({toggleMode, toggleMenu}) => {
  return (
    <header className={`w-full fixed top-0 z-20 h-14 flex items-center justify-between py-5 px-5 border-b dark:border-slate-700 bg-white dark:bg-slate-900`}>
        <figure className="flex items-center gap-3 w-8 h-8">
          <img src='./assets/atomo.png' alt="Logo Page" />
          <p className='font-medium text-2xl text-slate-800 max-small:hidden md:block dark:text-slate-100'> atomsdev </p>
        </figure>

        <div className="flex items-center gap-5">
          <HeaderLinks/> 

          <HeaderButton handleOnClick={toggleMode} />
          <BurguerMenu handleOnClick={toggleMenu} />
        </div>
    </header>
  );
};
export default Header;