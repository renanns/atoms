import { useEffect, useState } from 'react';

export default function ComponentHome() {
  const text = '  Dominando o Front-End e Transformando Ideias em Interfaces Incríveis!';
  const [buildText, setBuildText] = useState(''); // Usa o estado para armazenar a frase atual

  useEffect(() => {
    setBuildText('');
    let index = 0; // Índice para rastrear a posição atual

    const idInterval = setInterval(() => {
      if (index < text.length) {
        setBuildText((prev) => prev + text.charAt(index)); // Atualiza a frase
        index += 1;
      } else {
        clearInterval(idInterval);
      }
    }, 100);

    // Limpeza do intervalo ao desmontar o componente
    return () => clearInterval(idInterval);
  }, []);

  const scrollToSection = () => {
    const section = document.getElementById('Projetos');
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
    <div className="w-full h-screen flex items-center md:flex-col-reverse">
      <div className="w-1/2 h-full flex items-center justify-center md:w-full md:h-2/4">
        <div className="w-full h-3/4 flex flex-col justify-center gap-5 animate-slideDown md:1/2">
          <div className="flex flex-col gap-1">
            <p className="text-xl text-slate-500 italic md:text-lg sm:text-sm">
              Olá, eu sou
            </p>
            <h1 className="text-3xl text-slate-100 italic md:text-2xl sm:text-xl">
              Renan Nascimento.
            </h1>
            <h2 className="text-4xl font-bold text-center text-slate-400 md:text-3xl sm:text-2xl">
              {buildText}
            </h2>
          </div>
          <div className="w-full flex justify-evenly">
            <button
              onClick={scrollToSection}
              className="w-32 h-10 text-customBlue bg-tranparent border border-customBlue rounded font-bold"
            >
              Projetos
            </button>
            <button className="w-32 h-10 text-slate-100 bg-customBlue rounded">
              Download CV
            </button>
          </div>
        </div>
      </div>

      <div className="w-1/2 flex items-center justify-center md:w-96 md:h-96">
        <img src="./assets/astro.png" alt="Logo Home Page" className="animate-float" />
      </div>
    </div>
  );
}
