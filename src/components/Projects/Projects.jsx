import FooterSocial from "../FooterSocial/FooterSocial";

import { FiGithub } from "react-icons/fi";
import { IoLogoGitlab } from "react-icons/io5";

const Projects = () => {
    return (
        <div className="w-full h-full">
            <div className="w-full flex flex-col gap-5 h-2/6">
                <div className="relative mt-1">
                    <h1 className="text-5xl font-bold relative left-5 text-blue-200"> Projects </h1>
                    <h1 className="absolute bottom-4 text-5xl font-bold text-blue-500"> Projects </h1>
                </div>
                <p className="text-center pt-2 max-small:text-sm dark:text-slate-50">Conhecimentos já conquistados. Nesta seção, você encontrará tudo o que foi estudado, pesquisado e dominado, em uma jornada de aprendizado contínuo e dedicação incansável ao aprimoramento pessoal.</p>
            </div>

            <div className="w-full h-full flex flex-col py-5 px-5">
                <h1 className="text-center text-2xl pb-2  dark:text-slate-50"> Seção em andamento... </h1>
                <p className="text-center  dark:text-slate-50"> Para visualizar os projetos em código fonte acesse abaixo os repositórios.</p>
                <div className="w-full flex items-center justify-center gap-5 mt-10">
                    <FooterSocial
                        icon={<IoLogoGitlab size={30} color="3B82F6"/>}
                        link={"https://gitlab.com/renanns"}
                    />      

                    <FooterSocial
                        icon={<FiGithub size={30} color="3B82F6"/>}
                        link={"https://github.com/atomsdeveloper"}
                    />
  
                </div>     
            </div>
        </div>
    )
}
export default Projects;