import React from "react";

const FooterTitle = ({title}) => {
  return (
    <h1 className="text-center text-blue-500 font-bold text-2xl">{title}</h1>
  )
};
export default FooterTitle;
