import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

function Techs() {
    const techs = [
        {
          "name": "HTML",
          "image": "html.png",
          "desc": "Linguagem de marcação utilizada para estruturar o conteúdo na web. Permite a criação de documentos web com títulos, parágrafos, links, imagens e outros elementos."
        },
        {
          "name": "CSS",
          "image": "css.png",
          "desc": "Linguagem de estilo utilizada para definir a apresentação de documentos HTML. Permite a aplicação de estilos como cores, fontes, espaçamento e layout aos elementos HTML."
        },
        {
          "name": "JavaScript",
          "image": "javascript.png",
          "desc": "Linguagem de programação utilizada para criar interatividade em páginas web. Permite a criação de scripts que adicionam dinamismo e funcionalidade aos sites, como validação de formulários e animações."
        },
        {
          "name": "Tailwind",
          "image": "tailwind.png",
          "desc": "Framework CSS utilitário para estilização rápida e consistente. Facilita a criação de designs responsivos e modernos usando classes utilitárias pré-definidas."
        },
        {
          "name": "Typescript",
          "image": "typescript.png",
          "desc": "Superset tipado do JavaScript que adiciona tipos estáticos. Permite detectar erros de tipo durante o desenvolvimento, melhorando a robustez e a manutenção do código."
        },
        {
          "name": "Vue",
          "image": "vue.png",
          "desc": "Framework JavaScript para construção de interfaces de usuário. Facilita a criação de aplicações web interativas e dinâmicas com uma abordagem reativa e componentes reutilizáveis."
        },
        {
          "name": "React",
          "image": "react.png",
          "desc": "Biblioteca JavaScript para construção de interfaces de usuário. Permite a criação de componentes reutilizáveis e a construção de interfaces de usuário declarativas e eficientes."
        },
        {
          "name": "Redux",
          "image": "redux.png",
          "desc": "Biblioteca para gerenciamento de estado previsível em aplicações JavaScript. Facilita a gestão do estado da aplicação, permitindo a criação de aplicações mais previsíveis e testáveis."
        },
        {
          "name": "Node",
          "image": "node.png",
          "desc": "Ambiente de execução JavaScript server-side. Permite a criação de servidores e aplicações de back-end utilizando JavaScript, com alta performance e escalabilidade."
        },
        {
          "name": "Express",
          "image": "express.png",
          "desc": "Framework web minimalista para Node.js. Facilita a construção de servidores e APIs RESTful com um conjunto simples e robusto de funcionalidades."
        },
        {
          "name": "PostgreSQL",
          "image": "postgresql.png",
          "desc": "Sistema de gerenciamento de banco de dados relacional open-source. Oferece suporte a uma vasta gama de tipos de dados e funcionalidades avançadas, como consultas complexas e transações."
        },
        {
          "name": "MySQL",
          "image": "mysql.png",
          "desc": "Sistema de gerenciamento de banco de dados relacional popular. Utilizado amplamente em aplicações web para armazenar e gerenciar dados com alta performance e escalabilidade."
        },
        {
          "name": "Prisma",
          "image": "prisma.png",
          "desc": "ORM (Object-Relational Mapping) para Node.js e TypeScript. Facilita a interação com bancos de dados, permitindo a definição de modelos de dados e consultas de forma tipada."
        },
        {
          "name": "Docker",
          "image": "docker.png",
          "desc": "Plataforma para desenvolvimento, envio e execução de aplicações em contêineres. Permite criar ambientes isolados e consistentes para desenvolvimento, testes e produção, facilitando a portabilidade de aplicações."
        },
        {
          "name": "GitLab",
          "image": "gitlab.png",
          "desc": "Plataforma DevOps para gestão do ciclo de vida do desenvolvimento de software. Oferece ferramentas para versionamento de código, integração contínua, entrega contínua e monitoramento de projetos."
        }
    ]
      
    const settings = {
        className: "center",
        arrows: false,
        dots: false,
        infinite: true,
        speed: 1000,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
    };

  return (
    <div className="w-full h-full flex flex-col">
        <div className="relative mt-1 flex items-center justify-center">
            <h1 className="text-5xl font-bold relative left-5 text-blue-200"> Skills </h1>
            <h1 className="absolute bottom-4 text-5xl font-bold text-blue-500"> Skills </h1>
        </div>

        <Slider {...settings} className="w-full pt-5">
            {techs.map((tech) => (
                <div key={tech.name} className='flex gap-5'>
                    <img className="icon relative right-3/4" src={`/assets/${tech.image}`} alt={tech.name} />
                </div>
            ))}
        </Slider>

        <div className='w-full h-full text-center dark:text-slate-50 mt-3'>
            <h1> Sou um desenvolvedor full stack me especializado em tecnologias modernas para front-end e back-end. No front-end, minha expertise abrange JavaScript (ES6+), React e React Hooks, onde desenvolvo aplicações interativas e dinâmicas. Utilizo Next.js para criar aplicações robustas e otimizadas para SEO, aproveitando técnicas como Server-Side Rendering (SSR), Static Site Generation (SSG) e Incremental Static Regeneration (ISR). Tenho habilidades sólidas em HTML5 e CSS3, permitindo-me construir interfaces responsivas e acessíveis. Utilizo Styled-Components para escrever CSS dentro do JavaScript, promovendo componentes reutilizáveis e estilos dinâmicos. Escrevo testes unitários e de integração com Jest e Testing Library para garantir a qualidade e robustez das aplicações. Além disso, utilizo Storybook para criar e documentar componentes de UI de forma isolada, facilitando o desenvolvimento e a colaboração com designers. Também desenvolvo Progressive Web Apps (PWAs) que oferecem uma experiência similar à de aplicativos nativos.

            No back-end, minhas habilidades incluem desenvolvimento com Node.js e Express, criando servidores escaláveis e performáticos e APIs RESTful eficientes. Tenho experiência com diversos sistemas de gerenciamento de banco de dados, como MySQL/MariaDB para bancos de dados relacionais, MongoDB para armazenamento flexível e escalável de dados e PostgreSQL. Utilizo Next.js no back-end para criar aplicações full stack com uma integração entre servidor e cliente.Combinando essas tecnologias, sou capaz de criar aplicações completas, desde a concepção até a implementação e manutenção, garantindo alta performance, escalabilidade e uma excelente experiência do usuário. </h1>
        </div>
    </div>
  );
};
export default Techs;
