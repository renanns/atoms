const Knowledge = () => {
    return (
        <div className="w-full h-auto">

            <div className="w-full flex flex-col gap-5 h-2/6">
                <div className="relative">
                    <h1 className="text-5xl font-bold relative left-5 text-blue-200"> Knowledge </h1>
                    <h1 className="absolute bottom-4 text-5xl font-bold text-blue-500"> Knowledge </h1>
                </div>
                <p className="text-center pt-2 max-small:text-sm dark:text-slate-50">Conhecimentos já conquistados. Nesta seção, você encontrará tudo o que foi estudado, pesquisado e dominado, em uma jornada de aprendizado contínuo e dedicação incansável ao aprimoramento pessoal.</p>
            </div>
            <div className="w-full h-auto">

                <div className="w-full h-auto flex gap-4 py-5 px-5 max-medium:flex-col">
                    <div className="w-3/6 max-medium:w-full max-medium:h-3/6">
                        <div className="w-5/6 text-slate-400 flex flex-col items-start gap-2 rounded-lg bg-slate-100 h-full w-full shadow-md px-4 py-5">
                            <h1 className="font-extrabold text-blue-500 text-2xl"> 2022 - 2024 </h1>
                            <h1 className="text-slate-800 text-2xl"> BackEnd </h1>
                            <p className="text-slate-600"> Udemy </p>
                            <span  className="text-slate-700"> Javascript moderno (ES6+) para front-end (com Webpack, babel, React, React Hooks, Redux, HTML5, CSS3 e mais) e back-end (com Node, Express, MySQL / MariaDB, MongoDB, PostgreSQL, Next.Js, Strapi e mais) um(a) desenvolvedor(a) full stack.</span>
                        </div>
                    </div>
                    <div className="w-3/6 max-medium:w-full max-medium:h-3/6">
                        <div className="w-5/6 text-slate-400 flex flex-col items-start gap-2 rounded-lg bg-slate-100 h-full w-full shadow-md px-4 py-5">
                            <h1 className="font-extrabold text-blue-500 text-2xl"> 2021 - 2023 </h1>
                            <h1 className="text-slate-800 text-2xl"> FrontEnd </h1>
                            <p className="text-slate-600"> Udemy </p>
                            <span className="text-slate-500">React.Js + Next.Js completo do básico ao avançado. Aprenda ReactJS, NextJS, Styled-Components, testes com Jest e Testing Library, Server-Side Rendering e demais siglas (SSR, SSG, ISR, CSR, SPA, PWA, etc...) no front-end.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Knowledge;