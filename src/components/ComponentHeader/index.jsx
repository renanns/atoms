import ComponentNavLinks from '../ComponentNavLinks';

export default function ComponentHeader() {
  return (
    <header className="w-full h-20 md:h-32 flex md:flex-col md:gap-2 items-center fixed top-0 z-10 h-14 py-3 px-6 border-b border-slate-100 bg-slate-950 sm:justify-center sm:px-0">
      <figure className="flex items-center md:justify-center h-full w-1/4">
        <a href="#home">
          <img className="w-12 h-12" src="./assets/atomo.png" alt="Logo Page" />
        </a>
      </figure>

      <ComponentNavLinks />
    </header>
  );
}
