export default function ComponentNavLinks() {
  const links = document.querySelectorAll('nav a');

  const linksItems = ['Home', 'Quem sou', 'Projetos', 'Skills', 'Github', 'Contato'];

  links.forEach((link) => {
    link.addEventListener('click', () => {
      links.forEach((linkRemoveClass) => linkRemoveClass.classList.remove('active'));
      link.classList.add('active');
    });
  });

  return (
    <nav className="flex justify-between h-full w-full items-center md:justify-evenly md:w-full sm:h-14 sm:rounded-lg">
      <ul className="w-full h-full flex items-center justify-end md:justify-center gap-4 p-1 sm:gap-2">
        {linksItems.map((value, index) => (
          <li key={index}>
            <a
              href={`#${value.trim().replace(/\s+/g, '')}`}
              className={`relative block text-md text-slate-100 md:bg-slate-800 md:p-2 md:rounded sm:text-sm xsm:text-xs sm:px-1
                ${value == 'Home' ? 'active' : ''}`}
            >
              {value}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
}
