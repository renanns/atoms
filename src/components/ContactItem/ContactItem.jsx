import React from "react";

const ContactItem = ({icon, title, text}) => {
  return (
    <div className="w-full h-auto flex flex-col justify-center items-center gap-2">
        <div className="w-16 h-16 flex justify-center items-center rounded-full bg-blue-200"> 
            {icon}
        </div>
        <p className="text-md dark:text-slate-50"> {title} </p>
        <p className="text-sm dark:text-slate-50"> {text} </p>
      </div>
  )
};
export default ContactItem;
