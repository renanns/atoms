export default function ComponentProjects() {
  return (
    <div className="w-full h-screen flex flex-col gap-4 items-center bg-slate-800 p-4">
      <div className="w-full flex flex-col justify-center gap-2 h-22 mt-2">
        <div className="relative">
          <h1 className="text-2xl font-bold relative left-5 text-blue-50">
            Meus Projetos
          </h1>
          <h1 className="absolute bottom-3 text-2xl font-bold text-customBlue">
            Meus Projetos
          </h1>
        </div>
        <p className="text-base text-slate-500 italic sm:text-xs text-center">
          Conhecimentos já conquistados. Aqui você encontrará tudo o que foi estudado,
          pesquisado e dominado por mim em um aprendizado contínuo e dedicação até aqui.
        </p>
      </div>
      <div className="w-full h-full bg-slate-300">Testando update vercel</div>
    </div>
  );
}
