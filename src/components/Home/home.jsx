const Home  = () => {
    return (
        <div className='w-full h-full flex max-medium:flex-col'>
            <div className='w-3/6 flex items-center justify-center max-medium:w-full max-medium:h-4/6'>
              <div className="w-full h-full bg-home-atomo bg-center bg-no-repeat animate-float"></div>
            </div>
            <div className='w-3/6 max-medium:w-full max-medium:h-2/6 flex flex-col items-center justify-center gap-4'>
              <p className='dark:text-slate-50 text-slate-800'> <span className='text-blue-hover font-bold text-1xl italic pr-1'> I'm </span> Renan Nascimento da Silva </p>
              <h1 className='dark:text-slate-50 text-slate-800 font-bold text-center text-2xl'> DOMINANDO O FRONT-END & BACK-END E SE TORNANDO UM FULL STACK DE SUCESSO! </h1>
              <div className='w-full flex items-center justify-center gap-5'>
                <a href='#projects' 
                  className="w-36 h-10 mt-2 rounded font-bold tracking-wide flex items-center justify-center text-blue-500 bg-traparent border border-blue-500"> Projects! </a>

                <a href='#about' 
                   className="w-36 h-10 mt-2 rounded text-slate-50 font-bold tracking-wide flex items-center justify-center bg-blue-500"> Hire me! </a>
              </div>
            </div>      
          </div>
    );
};
export default Home;