import { MdDarkMode } from "react-icons/md";
import { MdLightMode } from "react-icons/md";

import { useSelector } from "react-redux";

export const HeaderButton  = ({handleOnClick}) => {
    const { dark } = useSelector(rootReducer => rootReducer.menuReducer);
    
    return (
        <button className="w-10 h-8 flex items-center justify-center rounded-full text-slate-50 dark:text-slate-700 bg-slate-300 dark:bg-slate-100"
          onClick={handleOnClick}>
            {dark ? <MdDarkMode size={20} color="#0F172A"/> : <MdLightMode size={20} color="#0F172A"/>}
        </button>
    );
};
