import React from "react";

const FooterSocial = ({icon, link}) => {
  return (
    <div className="w-14 h-14 flex items-center justify-center rounded-full bg-blue-200">
        <a href={`${link}`} target="_blank" rel="noopener noreferrer">
            {icon}
        </a>
    </div>
  )
};

export default FooterSocial;
