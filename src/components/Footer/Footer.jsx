import React from "react";

import FooterLinks from "../FooterLinks/FooterLinks";
import FooterSocial from "../FooterSocial/FooterSocial";
import FooterTitle from "../FooterTitle/FooterTitle";

import { FaArrowRightLong } from "react-icons/fa6";
import { TbCurrentLocation } from "react-icons/tb";
import { MdOutlinePhoneEnabled } from "react-icons/md";
import { RiMailSendLine } from "react-icons/ri";
import { RxInstagramLogo } from "react-icons/rx";
import { CiLinkedin } from "react-icons/ci";
import { FiGithub } from "react-icons/fi";
import { IoLogoGitlab } from "react-icons/io5";

const Footer = () => {
  return (
    <div className="w-full flex flex-col px-5 py-5">
        <div className={`w-full flex max-medium:flex-col gap-5 justify-around items-start`}>

            <div className="w-full h-auto flex flex-col gap-2">
                <FooterTitle title={"Agradecimento"} />
                <p className="text-center dark:text-slate-50">
                Obrigado por visitar meu portfólio. Para mais informações ou para discutir oportunidades de colaboração, entre em contato comigo!
                </p>
            </div>

            <div className="w-full h-auto flex flex-col">
                <FooterTitle title={"Links"} />

                <FooterLinks 
                    icon={<FaArrowRightLong size={20} color="#3B82F6"/>}
                    text={"Home"}
                    id={"#home"}
                />

                <FooterLinks 
                    icon={<FaArrowRightLong size={20} color="#3B82F6"/>}
                    text={"About"}
                    id={"#about"}
                />

                <FooterLinks 
                    icon={<FaArrowRightLong size={20} color="#3B82F6"/>}
                    text={"Knowledge"}
                    id={"#knowledge"}
                />

                <FooterLinks 
                    icon={<FaArrowRightLong size={20} color="#3B82F6"/>}
                    text={"Skills"}
                    id={"#skills"}
                />

                <FooterLinks 
                    icon={<FaArrowRightLong size={20} color="#3B82F6"/>}
                    text={"Projects"}
                    id={"#projects"}
                />

                <FooterLinks 
                    icon={<FaArrowRightLong size={20} color="#3B82F6"/>}
                    text={"Contact"}
                    id={"#contact"}
                />
            </div>

            <div className="w-full h-auto flex flex-col">
                <FooterTitle title={"Have a Question?"} />
                
                <FooterLinks 
                    icon={<TbCurrentLocation size={20} color="#3B82F6"/>}
                    text={"Campo Limpo - SP / Brasil"}
                />
                <FooterLinks 
                    icon={<MdOutlinePhoneEnabled size={20} color="#3B82F6"/>}
                    text={"(11) 94501-5885"}
                />
                <FooterLinks 
                    icon={<RiMailSendLine size={20} color="#3B82F6"/>}
                    text={"atomsfrontend@gmail.com"}
                />
            </div>

            <div className="w-full h-auto flex justify-around">
                <FooterSocial
                    icon={<RxInstagramLogo size={30} color="3B82F6"/>}
                    link={"https://www.instagram.com/irenannascimento/"}
                />

                <FooterSocial
                    icon={<CiLinkedin size={30} color="3B82F6"/>}
                    link={"https://www.linkedin.com/in/renannscimento/"}
                />

                <FooterSocial
                    icon={<IoLogoGitlab size={30} color="3B82F6"/>}
                    link={"https://gitlab.com/renanns"}
                />

                <FooterSocial
                    icon={<FiGithub size={30} color="3B82F6"/>}
                    link={"https://github.com/atomsdeveloper"}
                />

            </div>
        </div>

        <div className={`w-full h-auto flex items-center justify-center  px-5 py-5 mt-5 rounded-xl bg-slate-100`}>
            <span className="flex gap-2 items-center justify-center text-blue-500">This template is made with &#9829; by Renan Nascimento</span>
        </div>
    </div>
  )
};

export default Footer;
