import { TbCurrentLocation } from "react-icons/tb";
import { MdOutlinePhoneEnabled } from "react-icons/md";
import { RiMailSendLine } from "react-icons/ri";
import { IoEarthSharp } from "react-icons/io5";

import ContactItem from "../ContactItem/ContactItem";

const Contact = () => {
    return (
        <div className="w-full h-full">
            <div className="w-full flex flex-col gap-5 h-2/6">
                <div className="relative mt-1">
                    <h1 className="text-5xl font-bold relative left-5 text-blue-200"> Contact </h1>
                    <h1 className="absolute bottom-4 text-5xl font-bold text-blue-500"> Contact </h1>
                </div>
                <p className="text-center pt-2 max-small:text-sm dark:text-slate-50">Você está na seção de rodapé, onde pode encontrar links importantes, informações de contato e as redes sociais para acompanhar meu trabalho.</p>
            </div>
            <div className="w-full h-4/6 max-xmedium:h-auto">
                <div className={`w-full h-auto flex justify-around max-xmedium:flex-col items-center gap-3 py-5 mt-5`}>
                    <ContactItem 
                        icon={<TbCurrentLocation size={40} color="#3B82F6"/>}
                        title={"Address"}
                        text={"Campo Limpo - SP"}
                    />

                    <ContactItem 
                        icon={<MdOutlinePhoneEnabled  size={40} color="#3B82F6"/>}
                        title={"Phone Number"}
                        text={"(11) 94501-5885"}
                    />

                    <ContactItem 
                        icon={<RiMailSendLine size={40} color="#3B82F6"/>}
                        title={"Email Address"}
                        text={"atomsfrontend@gmail.com"}
                    />

                    <ContactItem 
                        icon={<IoEarthSharp size={40} color="#3B82F6"/>}
                        title={"Web Site"}
                        text={"atomsdev.com.br"}
                    />
                </div>
            </div>
        </div>
    )
}
export default Contact;